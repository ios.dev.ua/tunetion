//
//  URLRequestBuilder.swift
//  Tunetion
//
//  Created by Vladyslav Taranenko on 02.07.2023.
//

import Foundation

class URLRequestBuilder {
    private let url: URL
    private let method: HTTPMethod
    private var headers: HTTPHeaders?
    private var parameters: HTTPParameters?
    private var body: Data?

    init(
        url: URL,
        method: HTTPMethod,
        headers: HTTPHeaders? = nil,
        parameters: HTTPParameters? = nil,
        body: Data? = nil
    ) {
        self.url = url
        self.method = method
        self.headers = headers ?? [:]
        self.parameters = parameters ?? [:]
        self.body = body
    }

    public func build() -> (URLRequest?, Error?) {
        guard var components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
            let invalidURLParts = NSError(domain: "Failed to parse URLs constituent parts for \(url.self)", code: 0)
            return (nil, invalidURLParts)
        }

        if let parameters = parameters {
            components.queryItems = parameters.map {
                URLQueryItem(name: $0, value: $1)
            }
        }

        guard let url = components.url else {
            let invalidURL = NSError(domain: "URL \(url.self) not valid", code: 0)
            return (nil, invalidURL)
        }

        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.httpBody = body
        request.allHTTPHeaderFields = headers

        return (request, nil)
    }

    public func setBody(_ body: Data) {
        self.body = body
    }

    public func addHeader(_ header: String, forHTTPHeaderField: String) {
        headers?[forHTTPHeaderField] = header
    }

    public func addParameter(_ parameter: String, forHTTPParameterField: String) {
        parameters?[forHTTPParameterField] = parameter
    }
}
