//
//  TunetionApp.swift
//  Tunetion
//
//  Created by Vladyslav Taranenko on 01.07.2023.
//

import SwiftUI

@main
struct TunetionApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
