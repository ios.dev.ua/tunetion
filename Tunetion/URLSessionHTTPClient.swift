//
//  URLSessionHTTPClient.swift
//  Tunetion
//
//  Created by Vladyslav Taranenko on 01.07.2023.
//

import Foundation

class URLSessionHTTPClient: HTTPClient {
    private let session: URLSession
    private var receivedTasks = [URLRequest: URLSessionDataTask]()

    init() {
        let configuration = URLSessionConfiguration.default
        self.session = URLSession(configuration: configuration)
    }

    public func request(_ request: URLRequest, completion: @escaping RequisitionCompletion) {
        if let currentTask = receivedTasks[request] {
            currentTask.cancel()
        }

        let task = session.dataTask(with: request) { data, response, error in
            guard error == nil else {
                return completion(.failure(error!))
            }

            guard let response = response as? HTTPURLResponse else {
                let unexpectedError = NSError(domain: "HTTP failure response for \(self)", code: 0)
                return completion(.failure(unexpectedError))
            }

            completion(.success(data, response))
        }

        task.resume()
        receivedTasks[request] = task
    }
}
