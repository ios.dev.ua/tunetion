//
//  HTTPClient.swift
//  Tunetion
//
//  Created by Vladyslav Taranenko on 01.07.2023.
//

import Foundation

typealias HTTPHeaders = [String: String]
typealias HTTPParameters = [String: String]

public enum HTTPMethod: String {
    case GET
    case POST
}

public enum HTTPClientResult {
    case success(Data?, HTTPURLResponse)
    case failure(Error)
}

public protocol HTTPClient {
    typealias RequisitionCompletion = (HTTPClientResult) -> Void

    func request(_ request: URLRequest, completion: @escaping RequisitionCompletion)
}
