//
//  NetworkProvider.swift
//  Tunetion
//
//  Created by Vladyslav Taranenko on 02.07.2023.
//

import Foundation
import Network
import SwiftUI

class NetworkProvider: ObservableObject {
    @Published var isConnected: Bool = false
    private let monitor: NWPathMonitor

    init(monitor: NWPathMonitor = .init()) {
        self.monitor = monitor
    }

    public func start() {
        monitor.pathUpdateHandler = { [weak self] path in
            DispatchQueue.main.async {
                switch path.status {
                case .requiresConnection, .unsatisfied:
                    self?.isConnected = false
                case .satisfied:
                    self?.isConnected = true
                @unknown default:
                    break
                }
            }
        }
        monitor.start(queue: .global())
    }

    public func stop() {
        monitor.cancel()
    }
}
