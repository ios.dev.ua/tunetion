//
//  Endpoints.swift
//  Tunetion
//
//  Created by Vladyslav Taranenko on 02.07.2023.
//

import Foundation

enum Endpoints: String {
    case Testing = "api/test/testing"

    var url: URL {
        guard let url = URL(string: Environment.baseURL) else {
            preconditionFailure("URL \(Endpoints.self) not valid")
        }

        return url.appendingPathComponent(rawValue)
    }
}
