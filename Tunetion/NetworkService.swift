//
//  NetworkService.swift
//  Tunetion
//
//  Created by Vladyslav Taranenko on 02.07.2023.
//

import Foundation

class NetworkService {
    private let httpClient: HTTPClient
    private let networkProvider: NetworkProvider

    public enum Result<T> {
        case empty
        case success(T)
        case failure(Error)
    }

    init(httpClient: HTTPClient = URLSessionHTTPClient(), networkProvider: NetworkProvider = NetworkProvider()) {
        self.httpClient = httpClient
        self.networkProvider = networkProvider
    }

    public func request<T: Decodable>(
        _ url: URL,
        method: HTTPMethod = .GET,
        headers: HTTPHeaders? = nil,
        parameters: HTTPParameters? = nil,
        body: Encodable? = nil,
        completion: @escaping (NetworkService.Result<T>) -> Void
    ) {
        let requestBuilder = URLRequestBuilder(url: url, method: method, headers: headers, parameters: parameters)

        if !networkProvider.isConnected {
            let unstableConnection = NSError(domain: "Lost Internet connection", code: 0)
            return completion(.failure(unstableConnection))
        }

        if let body = body {
            do {
                let encoded = try JSONEncoder().encode(body)
                requestBuilder.setBody(encoded)
                requestBuilder.addHeader("application/json", forHTTPHeaderField: "Content-Type")
            } catch {
                completion(.failure(error))
            }
        }

        let (request, error) = requestBuilder.build()

        guard error == nil else {
            return completion(.failure(error!))
        }

        guard let request = request else {
            let unexpectedError = NSError(domain: "Request \(self) not valid", code: 0)
            return completion(.failure(unexpectedError))
        }

        httpClient.request(request) { result in
            switch result {
            case let .success(data, response):
                if T.self == Empty.self {
                    completion(.empty)
                    return
                }

                if let data = data, (200..<300).contains(response.statusCode) {
                    do {
                        let decoded = try JSONDecoder().decode(T.self, from: data)

                        return completion(.success(decoded))
                    } catch {
                        completion(.failure(error))
                    }
                }

                completion(.empty)
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
}
