//
//  Environment.swift
//  Tunetion
//
//  Created by Vladyslav Taranenko on 01.07.2023.
//

import Foundation

public enum Environment {
    private enum InfoKeys: String {
        case baseURL = "BASE_URL"
    }

    private static let infoDictionary: [String: Any] = {
        guard let plist = Bundle.main.infoDictionary else {
            fatalError("Plist not found")
        }
        return plist
    }()

    public static let baseURL: String = {
        guard let baseURL = Environment.infoDictionary[InfoKeys.baseURL.rawValue] as? String else {
            fatalError("Base URL not set in plist")
        }
        return baseURL
    }()
}
